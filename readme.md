## Simple framework simulation for uWSGI package

**sokol_router** - simulates routing function of the framework

**test_sokol_router** - simulates application which uses the **sokol_router** framework

## To Start uWSGI application test
```
uwsgi --ini my_uwsgi.ini
```
## UWSGI ini file
```
[uwsgi]
module = test_sokol_router   # application uses
http = :8000
```
