# -*- coding: utf-8 -*-
# routes table = {"GET" : {"/": func, "/api": func2}, "POST": {"/": func, "/api": func2} }

class Sokol:
    def __init__(self, routes):
        self.routes = routes
        self.response_status = "111"
        self.response_headers = [('Content-Type','text/html'), ]
        self.response_body = "INITIAL STATE"

    def response(self, status, body):
        self.response_status = status
        self.response_body = body

    def no_route(self):
        self.response_status = "404"
        self.response_body = "INVALID ROUTE"

    def no_method(self):
        self.response_status = "405"
        self.response_body = "INVALID METHOD"

    def router(self, environ, start_response):
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
        request_body = environ['wsgi.input'].read(request_body_size) if request_body_size else ""
        request_query = environ.get('QUERY_STRING', "")
        method_routes = self.routes.get(environ['REQUEST_METHOD'], None)
        if not method_routes:
            self.no_method()
        else:
            method_routes.get(environ['PATH_INFO'], self.no_route)(self, body=request_body, query=request_query)
        start_response(self.response_status, self.response_headers)
        return [self.response_body]
