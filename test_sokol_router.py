#!/usr/bin/env python3
import sokol_router

def get_root_view(sokol, query="", **kwargs):
    print('get_root_view is running, query=', query)
    sokol.response("200 OK", "<h2 style='color:blue'>GET ROOT, QUERY={}</h2>".format(query))

def get_api_view(sokol, query="", **kwargs):
    print('get_api_view is running, query=', query)
    sokol.response("200 OK", "<h2 style='color:red'>GET API, QUERY={}</h2>".format(query))

def post_root_view(sokol, body="", **kwargs):
    print('post_root_view is running')
    sokol.response("202 OK", "<h2 style='color:green'>POST ROOT BODY={}</h2>".format(body))

def post_api_view(sokol, body="", **kwargs):
    print('post_api_view is running')
    sokol.response("202 OK", "<h2 style='color:yellow'>POST API BODY={}</h2>".format(body))


routing_table = {"GET": {"/": get_root_view, "/api": get_api_view},
                 "POST": {"/": post_root_view, "/api": post_api_view},
                }

router = sokol_router.Sokol(routing_table)
application = router.router
